#!/bin/bash

#reading the old saved IP from config.txt
read MY_IP < "${BASH_SOURCE%/*}/config.txt"

#this is the domain name to be handeled. In case of free ddns this is the subdomain that the ddns offered to you like yourname.ddns.com
HOST=subdomain.example.com

#this is the user you have at the DDNS
USER=user_name_by_the_ddns

#this is the password that you have at the DDNS
#note that at least dyny.com requires that the password is MD5 hashed. 
#You can obtain a MD5 hash of your password (for dynu.com) here: 
#https://www.dynu.com/NetworkTools/Hash
PASS=password_by_the_ddns 

#this is how the external IP is obtained
#variant 1 - in case is not working comment this line and uncomment variant 2
NEW_IP="$(dig +short myip.opendns.com @resolver1.opendns.com)"

#variant 2 - in case variant 1 is not working comment variant 1 and uncomment variant 2
#NEW_IP="$(dig TXT +short o-o.myaddr.l.google.com @ns1.google.com)"

#in case verbose is not require the below 2 lines can be commented out
echo "My actual IP: ${NEW_IP}"
echo "The saved IP: ${MY_IP}"

#comparing the IPs
if [ "$NEW_IP" != "$MY_IP" ]
        then
                #verbose line below. can be commented out
                echo Update required.
                
                #this is the update query valid now only for dynu.com. 
                #this should be updated according to your DDNS API
                #myip at the end of the URL is left blank because dynu.com updates it automatically
                #in case required update the end of the URL with $NEW_IP, like this: myip=$NEW_IP
                UPDATE_QUERY="$(wget -qO- "https://api.dynu.com/nic/update?hostname=$HOST&username=$USER&password=$PASS&myip=")"
                
                #result of the query is logged
                echo "Time: $(date). $UPDATE_QUERY. From old IP: ${MY_IP} to new IP: ${NEW_IP}" >> "${BASH_SOURCE%/*}/log.txt"
                
                #new IP is saved in order to be used as new term of comparison
                echo "${NEW_IP}" > "${BASH_SOURCE%/*}/config.txt"
                echo Update executed. Check the log.txt from script directory for details.
        else
                #verbose line below. can be commented out
                echo IP valid.
                
                #the check is logged. In order to reduce the level of verbose into log the below line can be commented out
                echo "Time: $(date). Checked. Everything OK. Nothing updated." >> "${BASH_SOURCE%/*}/log.txt"

        fi
