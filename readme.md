<h1>INTRODUCTION</h1>
This is a linux bash script that helps with updating the (non fixed) external IP to a DDNS (Dynamic Domain Name Service).

This may prove to be useful in the situation when you are (self) hosting a (dev) server in a location without a dedicated IP. 
One solution would be to use a DDNS who would help you to relay your IP to a (sub)domain name. In such a case one would be dependend on the internet provider to ensure the link between the IP and the DDNS.

From my experience this is not always functioning.

Luckily the DDNS are offering through their APIs the possibility to to exactly this. For the ones who are having a Linux machine runing at their location I have wrote this script which is helping with the update of the IP.

At this moment the script supports the update for: <strong><a href="dynu.com" target="_blank">dynu.com</a></strong>. If you need support for other provider please let me know! 


<h1>INSTALLATION:</h1>

My recommendation and all the instructions below are to install this script as a normal user, in a subfolder of your user's home folder.
There is also the alternative to install it as a root user. In this case <code>/usr/local/bin/ddnsupdate</code> can be used as location to store the script. In this case all of the commands should be run as <code>root</code>

Here it goes. Create the directory for the script:
<pre>
cd ~
mkdir ddnsupdate
cd ddnsupdate
</pre>

Download the zip of this git and unzip it in the folder or clone it.

Configuration:

<pre>
nano ddnsupdate.sh
</pre>
<ul>
<li><strong>HOST</strong> is your (sub)domain like example.com or dev.example.com.</li>
<li><strong>USER</strong> is your user by the DDNS.</li>
<li><strong>PASS</strong> is the MD5 hash of your password by the DDNS. In case of dynu.com you can obtain it here: <a href="https://www.dynu.com/NetworkTools/Hash" target="_blank">https://www.dynu.com/NetworkTools/Hash</a></li>
</ul>
Update them accordingly and save the file.

Ensure that the file has the correct priviledges:

<pre>chmod 700 ddnsupdate.sh</pre>

Append the crontab in order to create a repeating updating task:
<pre>crontab -e</pre>
and create a new task by adding the following line at the end of the file:
<pre>*/360 * * * * /path/to/script/ddnsupdate.sh > /dev/null 2>&1</pre>

<strong>Attention:</strong>
<ul>
<li><strong>360</strong> is the interval in minutes when the task should be run. In this case it will run every 6 hours which should be pretty ok for a dev server. If your needs are more critical I advise you to reduce this to 5, 10 or 15 minutes.</li>
<li><strong>/path/to/script/</strong> should be update accordingly. If you followed the instructions should be something like /home/<code>user</code>/ddnsupdate</li>
</ul>

<h1>TESTING</h1>

The script logs the operation in the <code>log.txt</code> file stored in the same folder as the script. At installation the log should be empty.
The <code>config.txt</code> file is used to store the old IP address. This address is compared with the new one and their difference trigger the update.

To test the script one should just run it with the following command, while in the script directory (if not execute before <code>cd /path/to/script/</code>).
<pre>./ddnsupdate</pre>

First run of the script will trigger a "dry update" because there is no old IP to compare with. I left it like this for testin purposes. Whoever wants to disable this should only before running the script to write the exisint IP in the <code>config.txt</code> file.
The script verboses on the stdout the result but also the aux-files are updated. You can check the results (before and after) with:
<pre>
cat log.txt
cat config.txt
</pre>

Further on, based on the interval of the cron the log.txt will be updated. This can be checked with:
<pre>
cat log.txt
</pre>

<h1>CREDITS</h1>
Base Idea:
<ul>
<li>Basic script for DDNS update: <a href="https://gist.github.com/kylegibson/1273990/6117ccae7ed45195a2bc5cce54549d608c7c27bb" target="_blank">https://gist.github.com/kylegibson/1273990/6117ccae7ed45195a2bc5cce54549d608c7c27bb</a></li>
<li>Script for Dynu update for Mut@nt HD51 box with OpenATV 6.0: <a href="https://www.opena.tv/howtos/34464-dynu-com-ddns-automatic-update.html" target="_blank">https://www.opena.tv/howtos/34464-dynu-com-ddns-automatic-update.html</a></li>
</ul>
Obtaining the external IP script: <a href="https://www.cyberciti.biz/faq/how-to-find-my-public-ip-address-from-command-line-on-a-linux/" target="_blank">https://www.cyberciti.biz/faq/how-to-find-my-public-ip-address-from-command-line-on-a-linux/</a>

<h1>DISCLAIMER</h1>
<p>Before using this script please check also the tools that dynu.com is offering: <a href="https://www.dynu.com/en-US/Resources/Downloads" target="_blank">https://www.dynu.com/en-US/Resources/Downloads</a></p>

This script is given as it is without any warranty. Installation and utilization is done on own responsibility. 
I have mentioned already above the sources of inspiration for my code. If I will be notified of any copyright infrigements I will adapt this project accordingly
I am not assuming any licence for any of the code here and I am not asking for anything in return.
If you feel that this script is helping you and wish to give something back, please take a moment of consideration and <strong>pay it forwad!</strong>